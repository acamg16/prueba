// Carga las variables de entorno del archivo .env
require("dotenv").config();

// Importar paquetes
const express = require('express')	

// Importa la función de conexión
const { conexionBD } = require("./db/conexion");

//Crea la aplicación
const app = express();

//Importa la ruta
const tareasRoutes = require('./routes/tareas')
const inicioRoutes = require('./routes/inicio')


// Middleware
app.use(express.json());//Habilita el body en el router
app.use('/',inicioRoutes);
app.use('/tareas', tareasRoutes)


// Invoca la función de conexión a la BD
conexionBD();

//Inica el servidor
app.listen(3000)