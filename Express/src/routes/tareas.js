//Imporar los paquetes
const express = require('express');
const Tarea = require('../models/Tareas')

// Crea el router
const router = express.Router();

//DATA
const tareas = [
    { id: 1, titulo: 'correr' },
    { id: 2, titulo: 'leer' },
    { id: 3, titulo: 'estudiar' },
];

// Crea las rutas 

// Obtener todad las tareas
router.get('/', async (req, res) => {
    try {
        //Obtiene las tareas
        const tareas = await Tarea.find();

        res.status(200).send(tareas)

    } catch (error) {
        console.log(error);
        res.status(500).send({ mensaje: "Error del servidor" })
    }
});


// Obtener solo UNA Tarea
router.get('/:id', async (req, res) => {
    try {
        // Obtiene el parametro del URL
        const id = req.params.id;

        const tarea = await Tarea.findById(id);
        console.log(tarea);

        if (tarea != null) {
            // Encontro la tarea
            res.status(200).send(tarea);
        } else {
            // Retrona error 404
            res.status(404).send({
                mensaje: 'No se encontro la tarea',
            });
        }
    } catch (error) {
        console.error(error);
        res.status(500).send({ mensaje: 'Error del servidor' })
    }
})

// crear Una Tarea
router.post('/', async (req, res) => {

    try {
        // Obtiene el body
        const body = req.body;

        // Crea al documento
        const tarea = new Tarea({ titulo: body.titulo });

        //Guarda en la BD
        const resultado = await tarea.save();

        //Responde le cliente|
        res.status(201).send(resultado);

    } catch (error) {
        console.log(error);
        res.status(500).send({ mensaje: "Error del servidor" })
    }
})

//Modificar la tarea
router.patch("/:id", async (req, res) => {
    //Obtiene el ID de la tarea a Modificar
    const id = req.params.id;
    //Obtiene el body
    const body = req.body;
    //Obtiene la tarea
    try {
        const tarea = await Tarea.findOneAndUpdate({ _id: id }, body, { new: true, });
        if (tarea == null) {
            return res.status(404).send({ message: 'Tarea not found' });
        } else {
            res.status(200).send(tarea);

        }
    } catch (error) {
        console.log(error);
        res.status(500).send({ mensaje: 'Error de servidor' });
    }

})

// Elimina una tarea
router.delete('/:id', async(req, res) => {
    //Obtiene el ID de la tarea a Modificar
    const id = req.params.id;
    //Obtiene la tarea
    try {
        const tarea = await Tarea.deleteOne({ _id: id });
        res.status(200).send(tarea)
    } catch (error) {
        console.log(error);
        res.status(500).send({ mensaje: 'Error de servidor' });
    }
})
// exporta el router
module.exports = router;