let numeros1 = [];
console.log(numeros1);

let  numeros2 = [10, 20, 30];
console.log(numeros2);

let numeros3 = [10, 2.20, "cata",true];
console.log(numeros3);

console.log(numeros3[2]);

if(typeof numeros3[6]=="undefined"){
    console.log("No existe");
}

let datos1 = [1,2,3];
let datos2 = datos1;
datos1[0] = 10;
console.log(datos1);
console.log(datos2);

let num1 = [1,2,3];
let num2 = [...num1];// Copia 
num1[0] = 10;
console.log(num1);
console.log(num2);

console.log("Funciones");
let numbers =[10,20,30];
console.log(numbers);
//agregar al inicio
numbers.unshift(40);
console.log(numbers);
// agregar al final
numbers.push(20);
console.log(numbers);
// eliminar un elemento
numbers.shift(0);
console.log(numbers);
// agregar un elemento en posicion especifica
numbers.splice(1,0,80);
console.log(numbers);
// eliminar en un a posicion
numbers.splice(1,1);
console.log(numbers);

let emojis = ["😂","😃"];
console.log(emojis);

let nombres1 =["Universidad"];
let nombres2 =["Santo","Tomas"];
let nombres3 = nombres1.concat(nombres2);
console.log(nombres3); 

let elements = [100,200,300,500]
let nuevo1 = elements.map(e => e + 1);
console.log(elements);
console.log(nuevo1);

let nuevo2 = elements.map(e =>{
    let x = 0;
    x = e *3;
    return x;
});
console.log(nuevo2);

let r1 = elements.reduce((x,y) => x+y);
console.log(r1);

let arreglos1= [10,20,30,40];
console.log(arreglos1);

arreglos1.forEach(e => console.log(e));
arreglos1.forEach((e)=>{
    let x = e+3;
    console.log(x);
});

for(let e of arreglos1){
    console.log(e);
}
let arreglos2= [10,2,30,4];
//TODOSSSSS
let todosPares = arreglos2.every((e)=> e% 2===0);
console.log(todosPares);
//ALMENOS UNO
let algunoPar = arreglos2.some(e => e%2!=0);
console.log(algunoPar);

let par = true;
for(let i = 0; i < arreglos2.length;i++){
    if(arreglos2[i]% 2 ===0){
        par = true
    }else{
        par = false;
        break;
    }
}

console.log(par);
let impar = false;
for (let i = 0; i < arreglos2.length;i++){
    if(arreglos2[i]%2 != 0){
        impar = true;
        break;
    }
}
console.log(impar);

// Filtro
let notas = [2,3,1,4,5,2,3,4];
let aprobados = notas.filter((nota) => nota >= 3);
console.log(aprobados);





