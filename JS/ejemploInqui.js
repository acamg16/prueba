const inquirer = require("inquirer");

const menu = async () => {
  const opciones = [
    {
      name: "banda",
      type: "input",
      message: "Digite el nombre de la banda",
    },
    {
        name : 'opcion',
        type: "list",
        message: "Seleccionar opcion",
        choices: [
            {value: 1, name: "Agregar Banda"},
            {value: 2, name: "Eliminar Banda"},
            {value: 3, name: "Modificar Banda"},
            {value: 4, name: "ver Bandas"},
            {value: 5, name: "Salir"},

        ],
    }
  ];

  return inquirer.prompt(opciones);
};

async function main() {

  let opcion = await menu();
  console.log(opcion);
}
main();
