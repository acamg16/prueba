//Inicialización
let i = 1;
// While
while ( i <= 5){
    console.log(i);
    i++;
}

let numero = 1;
while(numero <= 30){
    if(numero% 2 ===0){
        console.log(`${numero} es par`);
    }
    numero++
}


// Do/while
let a = 0;
do{
    console.log(a);
    a++;

}while(a <= 5)

for (let x = 0; x < 5; x+=10) {
    console.log(x);
}