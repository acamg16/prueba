function mensaje(){
    console.log("Hola");
}

let message = () =>console.log("Hola");

let message2 = () => {
    let x = 10;
    console.log(x);
};

function mostrarNombre (nombre){
    console.log(`el nombre es ${nombre}`);
}

let showname = (name) => console.log(`The name is ${name}`);

function sumar (num1, num2){
    return num2 + num1;
}
let add = (num1, num2) => num1 + num2;
let getRandomValue= () =>{
    let random = Math.random();
    return random;
}
// Uso de la funcion 
mensaje();
message();
message2();
showname("KAMI");
mostrarNombre("Camilo");
console.log(`La suma es ${sumar(2,3)}`);
console.log(`La suma es ${add(20,3)}`);
console.log(getRandomValue());

let arr = [1,2,3,4]
function agregar(arreglo,dato) {
    arr.push(dato);
}
agregar(arr,5);
console.log(arr);

function incrementar(num) {
    return num++;
}
let f = 9;
incrementar(f);
console.log(f);