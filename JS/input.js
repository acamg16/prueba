const inquirer = require("inquirer");

const menu = async () => {
  const opciones = [

    {
        name : 'numero',
        type: "list",
        message: "Seleccionar opcion",
        choices: [
            {value: 1, name: "Agregar Banda"},
            {value: 2, name: "Eliminar Banda"},
            {value: 3, name: "Modificar Banda"},
            {value: 4, name: "ver Bandas"},
            {value: 5, name: "Salir"},

        ],
    }
  ];

  return inquirer.prompt(opciones);
};

module.exports = {
    menu,
}