// const {cargarBandas, agregarBandas,eliminarBanda} = require('./data');
// const {mostrarBandas} = require('./output');
const { menu } = require("./input");

async function main() {
  let opcion = { numero: 0 };
  // Ciclo principal de la aplicación
  while (opcion.numero != 5) {
    // Muestar el menu
    opcion = await menu();
    switch (opcion) {
      case 1:
        console.log("1");
        break;
      case 2:
        console.log("2");
        break;
      case 3:
        console.log("3");
        break;
      case 4:
        console.log("4");
        break;
      case 5:
        console.log("5");
        break;
      default:
        break;
    }
  }
  console.log(opcion);
}

main();
