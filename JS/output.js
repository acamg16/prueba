
function mostrarBandas (datos){
    for(const banda of datos.bands){
        console.log(`${banda.name}, ${banda.year}`);
    }
}

module.exports = {
    mostrarBandas,
}