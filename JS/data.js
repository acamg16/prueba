const fs = require('fs');
const path = require('path');

const ruta = path.join(__dirname, 'datos.json');


const cargarBandas = async  () =>{
    //Obtiene la ruta del archivo de datos
    // Verifica la ruta del archivo
    if(fs.existsSync(ruta)){
        //Abrir el archivo
        let archivo = fs.readFileSync(ruta);
        // Lee la información del archivo
        let datos = JSON.parse(archivo);
        //retorna la infromacion del archivo
        return datos;
    }
};

async function agregarBandas (datos, nuevaBanda){
    //console.log(datos);
    //console.log(nuevaBanda);
    //agregarBandas
    datos.bands.push(nuevaBanda);
    console.log(datos);
    // Convierte el objeto en string
    let cadena = JSON.stringify(datos);
    //console.log(cadena);
    // Escribe la cadena en el archivo JSON
    fs.writeFileSync(ruta, cadena);
    return datos;
}

async function eliminarBanda(datos, nombreBanda) {

    console.log(nombreBanda);    
    console.log(datos.bands);

    datos.bands = datos.bands.filter((b) => b.name != nombreBanda);

    let cadena = JSON.stringify(datos);
    //console.log(cadena);
    // Escribe la cadena en el archivo JSON
    fs.writeFileSync(ruta, cadena);
    return datos;
}

module.exports = {
    cargarBandas,
    agregarBandas,
    eliminarBanda
}