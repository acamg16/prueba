let x = 9;

let usuario = {
    nombre: "Pipe",
    apellido: "G",
    edad: 21,
    esAdulto: false,
    nombreCompleto(){
        return `${this.nombre} ${this.apellido}` ;
    }
}

console.log(usuario);
console.log(usuario.nombre);
console.log(usuario["nombre"]);
console.log(usuario.nombreCompleto());

usuario.apellido = '21';
usuario["nombre"] = 'Piso';
console.log(usuario);
// se agrega un parametro del objeto usuario
usuario.ciudad = 'Medallo MOR'
console.log(usuario);

delete usuario.ciudad;
console.log(usuario);

//Preguntar
let tieneCiudad = usuario.hasOwnProperty("ciudad");
console.log(tieneCiudad);
let tieneNombre = usuario.hasOwnProperty("nombre");
console.log(tieneNombre);

let bandas = [{
    nombre: 'AC/DC',
    pais: 'Australia'
},{
    nombre: "Guns ans Roses",
    pais:"USA"
},{
    nombre: "Queen",
    pais: "UK"
}]

let gringos = bandas.some((banda) => banda.pais ==="USA")
console.log(gringos);

usuario.notas = [3.4,4.8,1.5];
console.log(usuario);

usuario.ubicacion = {
    ciudad: "Medallo MOR",
    departamento: "Nartioquia",
    pais: "Polombia"
}
console.log(usuario.ubicacion.ciudad);
usuario.musica = bandas;
console.log(usuario);
console.log(usuario.musica[1].pais);

