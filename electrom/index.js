document.addEventListener("DOMContentLoaded", () => {
    const { ipcRenderer } = require("electron");

    // Escucha el evento "nueva-tarea"
    ipcRenderer.on("nueva-tarea", (evento, datos) => {
        // Obtiene la lista de tareas
        let tareas = document.getElementById("tareas");

        // Crea un elemento
        let nuevoElemento = document.createElement("li");
        nuevoElemento.innerHTML = datos.tarea;

        // Agrega el elemento a la lista
        tareas.appendChild(nuevoElemento);
    })
});  