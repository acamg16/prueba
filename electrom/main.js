// Libreria

const { Menu, ipcMain } = require("electron");
const { app, BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");
const { contextIsolated } = require("process");
const { title } = require("process");

let ventanaPrincipal;
let ventanaNuevaTarea;

function mostrarVentanaNuevaTarea() {
    ventanaNuevaTarea = new BrowserWindow({
        width: 400,
        height: 400,
        title: "Nueva Tarea"
    });
    ventanaNuevaTarea.loadURL(
        url.format({
            pathname: path.join(__dirname,"views/index2.html"),
            protocol: "file",
            slashes: true,
        })
    );
}

app.on("ready", () => {
    // Creación de la ventana principal
    ventanaPrincipal = new BrowserWindow({});
    // Carga del archivo index.html en la ventana
    ventanaPrincipal.loadURL(
        url.format({
            pathname: path.join(__dirname, "views/index.html"),
            protocol: "file",
            slashes: true,
        })
    );

    ipcMain.on("nueva-tarea", (evento, datos) => {

        // Envía los datos a index.html
        ventanaPrincipal.webContents.send('nueva-tarea', datos);

        // Cierra la ventana para crear la nueva tarea
        ventanaNuevaTarea.close();
    });

    const menuPrincipal =
        Menu.buildFromTemplate(templateMenu);
    Menu.setApplicationMenu(menuPrincipal);
});

function crearVentanaNuevaTarea() {
    // Crea la ventana de una nueva tarea
    ventanaNuevaTarea = new BrowserWindow({
        width: 400,
        height: 300,
        title: "Nueva tarea",
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        }
    });
    // Elimina el menu por defecto. SOLO WINDOWS Y LINUX
    ventanaNuevaTarea.setMenu(null);
    // Carga del archivo "nuevaTarea.html"
    ventanaNuevaTarea.loadURL(
        url.format({
            pathname: path.join(__dirname, "views/index2.html"),
            protocol: "file",
            slashes: true,
        })
    );
    ventanaNuevaTarea.on('closed', () => {
        // Libera la memoria de la ventana
        ventanaNuevaTarea = null;
    })
}
const templateMenu = [
    {
        label: "Tareas",
        submenu: [
            {
                label: "Nueva",
                accelerator: "Ctrl+N",
                click() {
                    crearVentanaNuevaTarea();
                    console.log("click");
                },
            },
        ],
    },

];

// Verifica que la aplicación 
// no esté empaquetada
if (!app.isPackaged) {
    // Agrega una nueva opción al menú
    templateMenu.push({
        label: "DevTools",
        submenu: [
            {
                label: "Mostrar/Ocultar Dev Tools",
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools();
                },
            },
            {
                role: "reload",
            },
        ],
    });
}