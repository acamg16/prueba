document.addEventListener("DOMContentLoaded", () => {
  const espacio = document.getElementById("espacio");
  const fecha = document.getElementById("fecha");
  console.log(espacio);

  //crear un nuevo elemento
  const nuevoDiv = document.createElement("div");
  nuevoDiv.textContent = "nuevo elemento";

  espacio.appendChild(nuevoDiv);

  const boton = document.getElementById("boton")
  console.log(boton);
  boton.addEventListener("click",()=>{
    console.log("click");
    const nuevaFecha= document.createElement("li")
    nuevaFecha.textContent = new Date();
    fecha.appendChild(nuevaFecha)
  })

  // console.log("Se cargó todo el DOM");
  // // Se obtiene el botón
  // const boton = document.getElementById("boton");
  // boton.addEventListener("click", (evento) => {
  //   console.log("CLICK");
  //   console.log(evento);
  // });
  // boton.addEventListener("mouseover", () => {
  //   console.log("MOUSEOVER");
  // });
  // boton.addEventListener("mouseout", () => {
  //   console.log("MOUSEOUT");
  // });

  // //Obtener el elemento
  // const area = document.getElementById("area")
  // // tener todos los hijos de un elemnto
  // const hijos = area.children;
  // console.log(hijos);

  // const ul = document.getElementById("personajes")
  // const personajes = ul.children;
  // console.log(personajes)

  // Array.from(personajes).forEach((p)=>{
  //   //Obtiene la primera letra del nombre
  //   const letra = p.textContent[0];
  //   if(letra ==="C"){
  //     p.classList.add("personaje")
  //   }
  // })
});
// //Obtener por ID
// const t = document.getElementById("titulo")
// console.log(t);
// //Obtener por taG
// const ciudades = document.getElementsByTagName("li");
// console.log(ciudades);
// //Obtener por clase
// const errores = document.getElementsByClassName("error")
// console.log(errores);

// //obtener por quuery selector
// const costa = document.querySelectorAll("li.costa")
// console.log(costa);
// //Obtiene el elemento
// const link = document.getElementById("link")
// //Cambiar el texto o contenido
// link.textContent="Universidad Santo Tomas"
// // cambiar u atributo
// link.setAttribute("href", "https://slides.com/johncardozo/dom#/6");

// //Cambiar imagen
// const imagen = document.getElementById("imagen")
// imagen.setAttribute("src","img/camp.jpeg")
// imagen.setAttribute("alt", "campo")
// imagen.setAttribute("width", "200")

// //obtener atriuto
// const href = link.getAttribute("href")
// const longitud = href.length;
// console.log(`la longitud del href es ${longitud}`);
// //Modificar el estilo
// link.style.color = "red";
// link.style.borderTop = "1px solid orange"

// const divError = document.getElementById("divError")
// divError.classList.add("error");

// const letra = document.getElementById("letra")
// let acertado = true;
// if(acertado){
//     letra.classList.add("correcto")
// }else{
//     letra.classList.add("error")
// }

// //eliminar una clase css de un elemento
// letra.classList.remove("correcto");
// letra.classList.remove("error")

// const boton = document.getElementById("boton")
// console.log(boton);
// //escuchar el evento
// boton.addEventListener("click",()=> console.log("me hicieron clik arrow!"))
// boton.addEventListener("click",function(){ console.log("click funcion anónima")})
// function mostra(){ console.log("clik en función externa")}
// boton.addEventListener("click",mostra)
